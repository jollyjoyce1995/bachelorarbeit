\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Einf\IeC {\"u}hrung}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Hintergrund und Relevanz}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Problemstellung}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Zielsetzung und Methodik}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Originalit\IeC {\"a}t}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Nicht-Ziele}{2}{section.1.5}
\contentsline {section}{\numberline {1.6}Aufbau der Arbeit}{2}{section.1.6}
\contentsline {chapter}{\numberline {2}Grundlagen}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Datenbanktypen}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Relationale Datenbanken}{3}{subsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.1}Aufbau}{3}{subsubsection.2.1.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.2}SQL (Structured Query Language)}{3}{subsubsection.2.1.1.2}
\contentsline {subsection}{\numberline {2.1.2}No-SQL Datenbanken}{4}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}Arten von NoSQL Datenbanken}{4}{subsubsection.2.1.2.1}
\contentsline {paragraph}{Key/Values Stores}{5}{section*.2}
\contentsline {paragraph}{Wide Column Stores}{5}{section*.3}
\contentsline {paragraph}{Document Stores}{5}{section*.4}
\contentsline {paragraph}{Graph Databases}{6}{section*.5}
\contentsline {section}{\numberline {2.2}Technische Grundlagen}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Skalierung}{6}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}CAP-Theorem}{7}{subsection.2.2.2}
\contentsline {paragraph}{}{7}{section*.6}
\contentsline {subsection}{\numberline {2.2.3}Transaktionen}{8}{subsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.3.1}ACID Eigenschaften}{8}{subsubsection.2.2.3.1}
\contentsline {subsubsection}{\numberline {2.2.3.2}BASE-Prinzip}{9}{subsubsection.2.2.3.2}
\contentsline {section}{\numberline {2.3}Persistenz Frameworks}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Hibernate OGM}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Spring Data}{11}{subsection.2.3.2}
\contentsline {chapter}{\numberline {3}Benchmark}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Testumgebung}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Testdaten}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Testf\IeC {\"a}lle}{15}{section.3.3}
\contentsline {section}{\numberline {3.4}Ergebnisse}{16}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Testfall 1: Anmelden/Registrieren}{16}{subsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.1.1}Datenmodell}{16}{subsubsection.3.4.1.1}
\contentsline {subsubsection}{\numberline {3.4.1.2}Ergebnis}{16}{subsubsection.3.4.1.2}
\contentsline {subsection}{\numberline {3.4.2}Testfall 2: Auflisten von Benutzer}{17}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}Datenmodell}{17}{subsubsection.3.4.2.1}
\contentsline {subsubsection}{\numberline {3.4.2.2}Ergebnis}{17}{subsubsection.3.4.2.2}
\contentsline {subsection}{\numberline {3.4.3}Testfall 3: Alterspyramide}{18}{subsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.3.1}Datenmodell}{18}{subsubsection.3.4.3.1}
\contentsline {subsubsection}{\numberline {3.4.3.2}Ergebnis}{18}{subsubsection.3.4.3.2}
\contentsline {subsection}{\numberline {3.4.4}Testfall 4: Profil bearbeiten}{19}{subsection.3.4.4}
\contentsline {subsubsection}{\numberline {3.4.4.1}Datenmodell}{19}{subsubsection.3.4.4.1}
\contentsline {subsubsection}{\numberline {3.4.4.2}Ergebnis}{19}{subsubsection.3.4.4.2}
\contentsline {subsection}{\numberline {3.4.5}Testfall 5: Auflistung aller M\IeC {\"a}nner}{20}{subsection.3.4.5}
\contentsline {subsubsection}{\numberline {3.4.5.1}Datenmodell}{20}{subsubsection.3.4.5.1}
\contentsline {subsubsection}{\numberline {3.4.5.2}Ergebnis}{20}{subsubsection.3.4.5.2}
\contentsline {section}{\numberline {3.5}Gesamt\IeC {\"u}berblick zum Performanz-Vergleich}{21}{section.3.5}
\contentsline {chapter}{\numberline {4}Fazit}{22}{chapter.4}
\contentsline {section}{\numberline {4.1}Diskussion und Reflexion}{22}{section.4.1}
\contentsline {section}{\numberline {4.2}Zusammenfassung}{22}{section.4.2}
\contentsline {section}{\numberline {4.3}Related Work}{23}{section.4.3}
\contentsline {section}{\numberline {4.4}Future Work}{23}{section.4.4}
\contentsline {chapter}{\numberline {A}Spring Data}{24}{appendix.A}
\contentsline {chapter}{\numberline {B}Hibernate}{35}{appendix.B}
\contentsline {chapter}{\numberline {C}Verzeichnisse}{48}{appendix.C}
\contentsline {section}{Abbildungsverzeichnis}{48}{appendix.C}
\contentsline {section}{Tabellenverzeichnis}{50}{appendix*.7}
\contentsline {section}{Literaturverzeichnis}{51}{appendix*.8}
